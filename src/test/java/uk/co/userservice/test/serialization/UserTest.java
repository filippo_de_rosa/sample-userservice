package uk.co.userservice.test.serialization;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import io.dropwizard.jackson.Jackson;

import org.junit.Test;
import uk.co.userservice.api.User;

import java.util.LinkedHashMap;

import static io.dropwizard.testing.FixtureHelpers.fixture;
import static org.assertj.core.api.Assertions.assertThat;

/**
 * Class to test serialization and deserialization of the User entity
 */
public class UserTest {

    private static final ObjectMapper MAPPER = Jackson.newObjectMapper();

    @Test
    public void serializesToJSON() throws Exception {
        final User user = createTestUser();

        final String expected = MAPPER.writeValueAsString(MAPPER.readValue(fixture("fixtures/testuser.json"), new TypeReference<LinkedHashMap<String, Object>>() {}));
        assertThat(MAPPER.writeValueAsString(user)).isEqualToIgnoringWhitespace(expected);
    }

    @Test
    public void deserializesToJSON() throws Exception {
        final User user = createTestUser();
        assertThat(MAPPER.readValue(fixture("fixtures/testuser.json"), User.class)).isEqualTo(user);
    }

    private User createTestUser() {
        return new User("testname","testlastname", "testuser", "mypwd", "test@test.com", "uk");
    }

}
