package uk.co.userservice.test.resources;

import com.fasterxml.jackson.databind.ObjectMapper;
import io.dropwizard.jackson.Jackson;
import io.dropwizard.testing.junit.ResourceTestRule;
import org.junit.*;
import org.junit.rules.ExpectedException;
import uk.co.userservice.api.User;
import uk.co.userservice.persistence.UserDB;
import uk.co.userservice.resources.UserService;

import javax.ws.rs.ClientErrorException;
import javax.ws.rs.NotFoundException;
import javax.ws.rs.client.Entity;
import javax.ws.rs.core.MediaType;
import java.util.ArrayList;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.*;

/**
 * Class to test User Service apis
 */
public class UserServiceTest {

    private static final UserDB userDB = mock(UserDB.class);

    private static final ObjectMapper MAPPER = Jackson.newObjectMapper();

    @Rule
    public ExpectedException exception = ExpectedException.none();

    @ClassRule
    public static final ResourceTestRule resources = ResourceTestRule.builder()
            .addResource(new UserService("http://localhost:8080/", userDB)).build();

    private final User user = createTestUser();
    private final List<User> users = getUserList();
    private final List<User> ukUsers = getUserFromUnitedKingdom();

    @Before
    public void setup() {
        when(userDB.getById("test@test.com")).thenReturn(user);
        when(userDB.getAll()).thenReturn(users);
        when(userDB.findByCountry("uk")).thenReturn(ukUsers);
    }

    @After
    public void tearDown() {
        // Reset the mock after each test because of the @ClassRule
        reset(userDB);
    }

    @Test
    public void testGetUser() throws Exception {
        assertThat(resources.client().target("/user/get/test@test.com").request().get(User.class)).isEqualTo(user);
        verify(userDB).getById("test@test.com");
    }

    @Test
    public void testGetUserNotFound() throws Exception {
        exception.expect(NotFoundException.class);
        resources.client().target("/user/get/notexisting@email.com").request().get(User.class);
    }

    @Test
    public void testAddUser() throws Exception{

        final User newUser = new User("newName", "newLastname", "newNickname", "password", "newmail@email.com", "uk");
        resources.client().target("/user/add").request()
                .put(Entity.entity(newUser, MediaType.APPLICATION_JSON_TYPE), User.class);
        verify(userDB).save(newUser);
    }

    @Test
    public void testAddUserReturnConflict() throws Exception{
        try {
            resources.client().target("/user/add").request()
                    .put(Entity.entity(user, MediaType.APPLICATION_JSON_TYPE), User.class);
        } catch (final ClientErrorException cee) {
            assertThat(cee.getResponse().getStatus()).isEqualTo(409);
        }
    }

    @Test
    public void testRemoveUser() throws Exception{
        resources.client().target("/user/remove/test@test.com").request().delete(User.class);
        verify(userDB).remove("test@test.com");
    }

    @Test
    public void testRemoveUserNotFound() throws Exception{
        exception.expect(NotFoundException.class);
        resources.client().target("/user/remove/notexistinguser@email.com").request().delete(User.class);
    }

    @Test
    public void testUpdateUser() throws Exception{

        resources.client().target("/user/update").request()
                .put(Entity.entity(user, MediaType.APPLICATION_JSON_TYPE), User.class);
        verify(userDB).save(user);
    }

    @Test
    public void testUpdateUserNotFound() throws Exception{
        final User newUser = new User("newName", "newLastname", "newNickname", "password", "newmail@email.com", "uk");

        exception.expect(NotFoundException.class);
        resources.client().target("/user/update").request()
                .put(Entity.entity(newUser, MediaType.APPLICATION_JSON_TYPE), User.class);
    }

    @Test
    public void testGetAllUsers() throws Exception{
        String result = resources.client().target("/user/all").request().get(String.class);
        verify(userDB).getAll();
        String expected = MAPPER.writeValueAsString(users);
        assertThat(result).isEqualTo(expected);
    }

    @Test
    public void testGetUsersByCountry() throws Exception{
        String result = resources.client().target("/user/by-country/uk").request().get(String.class);
        verify(userDB).findByCountry("uk");
        String expected = MAPPER.writeValueAsString(ukUsers);
        assertThat(result).isEqualTo(expected);
    }

    @Test
    public void testGetUsersByCountryReturnsEmptyList() throws Exception{
        List result = resources.client().target("/user/by-country/fr").request().get(List.class);
        verify(userDB).findByCountry("fr");
        assertThat(result).isEmpty();
    }

    private User createTestUser() {
        return new User("testname", "testlastname", "testuser", "mypwd", "test@test.com", "uk");
    }

    private List<User> getUserList() {

        final List<User> users = new ArrayList<>();

        users.add(new User("testname", "testlastname", "testuser", "mypwd", "test@test.com", "uk"));
        users.add(new User("testname2", "testlastname2", "testuser2", "mypwd", "test2@test.com", "it"));
        users.add(new User("testname3", "testlastname3", "testuser3", "mypwd", "test3@test.com", "fr"));
        users.add(new User("testname4", "testlastname4", "testuser4", "mypwd", "test4@test.com", "es"));

        return users;
    }

    private List<User> getUserFromUnitedKingdom() {

        final List<User> users = new ArrayList<>();

        users.add(new User("testname", "testlastname", "testuser", "mypwd", "test@test.com", "uk"));
        users.add(new User("testname2", "testlastname2", "testuser2", "mypwd", "test2@test.com", "uk"));

        return users;
    }
}