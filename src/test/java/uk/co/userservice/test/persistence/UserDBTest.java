package uk.co.userservice.test.persistence;

import org.junit.Before;
import org.junit.Test;
import uk.co.userservice.api.User;
import uk.co.userservice.persistence.UserDB;

import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;

/**
 * Integration test the UserDB persistence class
 */
public class UserDBTest {

    UserDB userDao;

    @Before
    public void setup(){
        userDao = new UserDB();
    }

    @Test
    public void testGetUser(){
        User user = userDao.getById("firstname.lastname1@email.com");

        assertThat(user.getFirstName()).isEqualTo("FirstName1");
        assertThat(user.getLastName()).isEqualTo("Lastname1");
        assertThat(user.getNickname()).isEqualTo("fnln1");
    }

    @Test
    public void testGetUserNotExisting(){
        User user = userDao.getById("notexisting");

        assertThat(user).isNull();
    }

    @Test
    public void testAddUser(){
        User newUser = new User("Test", "User", "testuser", "mypwd", "testuser@email.com", "uk");

        userDao.save(newUser);

        User theUser = userDao.getById("testuser@email.com");

        assertThat(theUser.getFirstName()).isEqualTo("Test");
        assertThat(theUser.getLastName()).isEqualTo("User");
        assertThat(theUser.getNickname()).isEqualTo("testuser");
        assertThat(theUser.getPassword()).isEqualTo("mypwd");
        assertThat(theUser.getEmail()).isEqualTo("testuser@email.com");
        assertThat(theUser.getCountry()).isEqualTo("uk");

        assertThat(userDao.getCount()).isEqualTo(4);
    }

    @Test
    public void testRemoveUser(){
        assertThat(userDao.getCount()).isEqualTo(4);
        userDao.remove("firstname.lastname1@email.com");
        assertThat(userDao.getCount()).isEqualTo(3);
    }

    @Test
    public void testFindUserFromUk(){
        final List<User> ukUsers = userDao.findByCountry("uk");

        assertThat(ukUsers).isNotEmpty();
        assertThat(ukUsers.size()).isEqualTo(2);
        assertThat(ukUsers.get(0).getNickname()).isEqualTo("fnln2");
    }

    @Test
    public void testFindUserNoCountry(){
        final List<User> ukUsers = userDao.findByCountry("ch");
        assertThat(ukUsers).isEmpty();
    }
}
