package uk.co.userservice;

import io.dropwizard.Application;
import io.dropwizard.setup.Environment;
import uk.co.userservice.health.ServiceCheck;
import uk.co.userservice.persistence.UserDB;
import uk.co.userservice.resources.CompetitionService;
import uk.co.userservice.resources.SearchService;
import uk.co.userservice.resources.UserService;


/**
 * Main application class for the User Service
 */
public class UserServiceApplication extends Application<UserServiceConfiguration>
{

	public static void main(String[] args) throws Exception
	{
		new UserServiceApplication().run(args);
	}

	@Override
	public void run(final UserServiceConfiguration config, final Environment env) throws Exception
	{
		final UserDB userDao = new UserDB();

		final UserService theUserService = new UserService(config.getHost(), userDao);
		env.jersey().register(theUserService);

		final SearchService theSearchService = new SearchService();
		env.jersey().register(theSearchService);

		final CompetitionService theCompetitionService = new CompetitionService();
		env.jersey().register(theCompetitionService);

		final ServiceCheck theServiceCheck = new ServiceCheck(config.getVersion(), userDao);
		env.healthChecks().register("userStatus", theServiceCheck);
		env.jersey().register(theServiceCheck);
	}
}
