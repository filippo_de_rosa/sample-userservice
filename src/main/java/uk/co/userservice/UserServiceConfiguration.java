package uk.co.userservice;

import com.fasterxml.jackson.annotation.JsonProperty;
import io.dropwizard.Configuration;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;



/**
 * Configuration class for the User service
 */
public class UserServiceConfiguration extends Configuration
{

	@Valid
	@NotNull
	@JsonProperty
	private String version;

	@Valid
	@NotNull
	@JsonProperty
	private String host;

	public String getVersion()
	{
		return version;
	}

	public String getHost()
	{
		return host;
	}
}
