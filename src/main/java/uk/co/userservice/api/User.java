package uk.co.userservice.api;


import com.fasterxml.jackson.annotation.JsonProperty;
import org.apache.commons.lang3.builder.EqualsBuilder;


/**
 * POJO class to represent the User entity
 */
public class User
{

	@JsonProperty
	private String firstName;

	@JsonProperty
	private String lastName;

	@JsonProperty
	private String nickname;

	@JsonProperty
	private String password;

	@JsonProperty
	private String email;

	@JsonProperty
	private String country;

	public User()
	{
		// Jackson deserialization
	}

	public User(final String firstName, final String lastName, final String nickname, final String password, final String email,
			final String country)
	{
		this.firstName = firstName;
		this.lastName = lastName;
		this.nickname = nickname;
		this.password = password;
		this.email = email;
		this.country = country;
	}

	public String getFirstName()
	{
		return firstName;
	}

	public void setFirstName(final String firstName)
	{
		this.firstName = firstName;
	}

	public String getLastName()
	{
		return lastName;
	}

	public void setLastName(final String lastName)
	{
		this.lastName = lastName;
	}

	public String getNickname()
	{
		return nickname;
	}

	public void setNickname(final String nickname)
	{
		this.nickname = nickname;
	}

	public String getPassword()
	{
		return password;
	}

	public void setPassword(final String password)
	{
		this.password = password;
	}

	public String getEmail()
	{
		return email;
	}

	public void setEmail(final String email)
	{
		this.email = email;
	}

	public String getCountry()
	{
		return country;
	}

	public void setCountry(final String country)
	{
		this.country = country;
	}

	@Override
	public boolean equals(Object object) {
		if (!(object instanceof User)){
			return false;
		}

		if (object == this){
			return true;
		}

		final User anotherUser = (User) object;
		return new EqualsBuilder().append(this.firstName, anotherUser.firstName).append(this.lastName, anotherUser.lastName)
				.append(this.nickname, anotherUser.nickname).append(this.email, anotherUser.email)
				.append(this.country, anotherUser.country).isEquals();
	}
}
