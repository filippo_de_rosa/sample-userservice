package uk.co.userservice.resources;

import org.apache.commons.lang3.StringUtils;
import org.glassfish.jersey.server.ManagedAsync;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import uk.co.userservice.api.User;

import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.container.AsyncResponse;
import javax.ws.rs.container.Suspended;


/**
 * Class that exposes the Search services API
 */
@Path("/search")
public class SearchService
{

	private static final Logger LOGGER = LoggerFactory.getLogger(SearchService.class);

	@POST
	@ManagedAsync
	@Path("/handle-new-user")
	public void handleNewUser(@Suspended final AsyncResponse asyncResponse, final User user)
	{
		final String msg = String.format("Added new user: %s %s %s", user.getNickname(), user.getFirstName(), user.getLastName());
		LOGGER.info(msg);
		asyncResponse.resume(StringUtils.EMPTY);
	}
}
