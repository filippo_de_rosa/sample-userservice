package uk.co.userservice.resources;

import com.codahale.metrics.annotation.Timed;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import uk.co.userservice.api.User;
import uk.co.userservice.persistence.UserDB;

import javax.ws.rs.*;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Entity;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.List;


/**
 * Class exposing the UserService services
 */
@Path("/user")
public class UserService
{

	private static final Logger LOGGER = LoggerFactory.getLogger(UserService.class);

	private String host;

	private UserDB userDao;

	public UserService(final String host, final UserDB userDao)
	{
		this.host = host;
		this.userDao = userDao;
	}

	public String getHost()
	{
		return host;
	}

	public UserDB getUserDao() {
		return userDao;
	}

	@GET
	@Timed
	@Path("/get/{emailId}")
	@Produces(MediaType.APPLICATION_JSON)
	public User get(@PathParam("emailId") final String emailId)
	{
		final User theUser = getUserDao().getById(emailId);
		if (theUser == null)
		{
			final String msg = String.format("User with id %s not found", emailId);
			LOGGER.error(msg);
			throw new WebApplicationException(msg, Response.Status.NOT_FOUND);
		}
		return theUser;
	}

	@PUT
	@Timed
	@Path("/add")
	@Consumes(MediaType.APPLICATION_JSON)
	public Response add(final User user)
	{
		final User theUser = getUserDao().getById(user.getNickname());
		if (theUser != null)
		{
			final String msg = String.format("User with Nickname %s already exists", user.getNickname());
			LOGGER.error(msg);
			throw new WebApplicationException(msg, Response.Status.CONFLICT);
		}
		getUserDao().save(user);
		// Notifying the search service about the new user
		WebTarget target = ClientBuilder.newClient().target(getHost());
		target.path("/search/handle-new-user").request().async().post(Entity.entity(user, MediaType.APPLICATION_JSON_TYPE));

		return Response.ok().build();
	}

	@DELETE
	@Timed
	@Path("/remove/{emailId}")
	public Response remove(@PathParam("emailId") final String emailId) {
		final User theUser = getUserDao().getById(emailId);
		if (theUser == null)
		{
			final String msg = String.format("User with id %s not found", emailId);
			LOGGER.warn(msg);
			throw new WebApplicationException(msg, Response.Status.NOT_FOUND);
		}
		getUserDao().remove(emailId);

		return Response.ok().build();
	}

	@PUT
	@Timed
	@Path("/update")
	public Response updateUser(final User user)
	{
		final User theUser = getUserDao().getById(user.getEmail());
		if (theUser == null)
		{
			final String msg = String.format("User with Nickname %s not found", user.getNickname());
			LOGGER.warn(msg);
			throw new WebApplicationException(msg, Response.Status.NOT_FOUND);
		}

		getUserDao().save(user);

		if (!StringUtils.equals(user.getNickname(), theUser.getNickname())){
			// Notifying the competition service about the change of nickname
			String msg = String.format("User with id: %s updated nickname from %s to %s", theUser.getEmail(), theUser.getNickname(), user.getNickname());
			LOGGER.info(msg);
			WebTarget target = ClientBuilder.newClient().target(getHost());
			target.path("/competition/refresh-usernickname").request().async()
					.post(Entity.entity(user, MediaType.APPLICATION_JSON_TYPE));
		}

		return Response.ok().build();
	}

	@GET
	@Timed
	@Path("/all")
	@Produces(MediaType.APPLICATION_JSON)
	public List<User> getUsers() {
		return getUserDao().getAll();
	}

	@GET
	@Timed
	@Path("/by-country/{countrycode}")
	@Produces(MediaType.APPLICATION_JSON)
	public List<User> findUsersByCountry(@PathParam("countrycode") final String countryCode) {
		return getUserDao().findByCountry(countryCode);
	}

}
