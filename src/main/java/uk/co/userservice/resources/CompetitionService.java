package uk.co.userservice.resources;

import org.apache.commons.lang3.StringUtils;
import org.glassfish.jersey.server.ManagedAsync;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import uk.co.userservice.api.User;

import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.container.AsyncResponse;
import javax.ws.rs.container.Suspended;


/**
 * Class that exposes the Competition services API
 */
@Path("/competition")
public class CompetitionService
{
	private static final Logger LOGGER = LoggerFactory.getLogger(CompetitionService.class);

	@POST
	@ManagedAsync
	@Path("/refresh-usernickname")
	public void updateNickname(@Suspended final AsyncResponse asyncResponse, final User user)
	{
		final String msg = String.format("User with id: %s has updated nickname to: %s", user.getEmail(), user.getNickname());
		LOGGER.info(msg);
		asyncResponse.resume(StringUtils.EMPTY);
	}
}
