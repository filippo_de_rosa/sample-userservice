package uk.co.userservice.health;

import com.codahale.metrics.health.HealthCheck;
import uk.co.userservice.persistence.UserDB;


/**
 * Class that provides health check information on the User Service
 */
public class ServiceCheck extends HealthCheck
{
	private final String version;

	private final UserDB userDao;

	public ServiceCheck(final String version, final UserDB userDao)
	{
		this.version = version;
		this.userDao = userDao;
	}

	protected Result check() throws Exception
	{
		return Result.healthy("UserService version: %s, total number of users: %s", this.version, this.userDao.getCount());
	}
}
