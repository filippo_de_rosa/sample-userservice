package uk.co.userservice.persistence;

import org.apache.commons.lang3.StringUtils;
import uk.co.userservice.api.User;

import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;


/**
 * Persistence class for the User entity
 */
public class UserDB
{
	private static Map<String, User> users = new HashMap<>();

	static
	{
		users.put("firstname.lastname1@email.com", new User("FirstName1", "Lastname1", "fnln1", "mypwd", "firstname.lastname1@email.com", "it"));
		users.put("firstname.lastname2@email.com", new User("FirstName2", "Lastname2", "fnln2", "mypwd", "firstname.lastname2@email.com", "uk"));
		users.put("firstname.lastname3@gmail.com", new User("FirstName3", "Lastname3", "fnln3", "mypwd", "firstname.lastname3@gmail.com", "es"));
	}

	public User getById(final String emailId)
	{
		return users.get(emailId);
	}

	public List<User> getAll()
	{
		return users.entrySet().stream().map(Map.Entry::getValue).collect(Collectors.toList());
	}

	public int getCount()
	{
		return users.size();
	}

	public void remove(final String emailId)
	{
		if (!users.isEmpty() && users.containsKey(emailId))
		{
			users.remove(emailId);
		}
	}

	public void save(final User newUser)
	{
		users.put(newUser.getEmail(), newUser);
	}

	public List<User> findByCountry(final String countryCode)
	{
		if (users.isEmpty())
		{
			return Collections.emptyList();
		}
		return users.entrySet().stream().filter(userEntry -> StringUtils.equalsIgnoreCase(userEntry.getValue().getCountry(),
				countryCode)).map(Map.Entry::getValue).collect(Collectors.toList());
	}
}
