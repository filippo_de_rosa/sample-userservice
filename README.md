# Sample Users Service

The Sample Users Service application was developed to, as its name implies, provide a small Users microservice example to manage users.

# Overview & Architecture

The application allows client to perform different operations on users stored on the persistence level:

* add a new user (identified uniquely by his/her email)
* get a user
* get all the users
* remove a user
* update a user
* get users by country Isocode

The `UserService` class defines the API offered for the Users Service that will allow clients to perform the operations mentioned above. Later all the API will be explained in details.

There are other two services defined in the application, the `SearchService` and the `CompetitionService`. These two services simulate a more complex architecture where the Users Service needs to interact with other services.

Each time the add a new user operation is performed successfully, the Search Service will be notified about the new user that has been added. The communication will happen asynchronously to decouple the services in order to preserve the autonomy of the service and also to improve scaleability.

Also the Competition Service will be notified whenever a user updates his `nickname`. Similarly to the Search Service the notification will happen asynchronously.

In a real production environment the Search and Competition service would be two services developed and deployed independently from the Users Service.

The persistence layer is offered by the `UserDB` class which is using a mock database implementation, i.e. users are stored in memory and operations performed will be discarded once the application is restarted.

The framework used to implement the Users Service is Dropwizard. Dropwizard it's a rapid and lightweight framework for getting a web service up and running. It's an ecosystem which contains all the dependencies (such as Jersey, jackson or jetty) bundled into single package or can be added as separate module. The language used by Dropwizard is Java.

The reason to choose Dropwizard were some of the features like:

1. **Jetty**: You would require HTTP for running a web application. Dropwizard embeds the Jetty servlet container for running the web applications. This allowed to create the Users Service and deploy it without the need of the installation of any external webservers.

2. **Jersey**: Jersey is one of the best REST API implementation out in the market. Also it follows the standard JAX-RS specification and it is the reference implementation for the JAX-RS specification.

3. **Jackson**: Jackson is the defacto standard for JSON format handling. It is one of the best object mapper API for the JSON format.

# Structure

The `src` folders contains all the sources for the Sample User Service project. Inside there are other two folders:

* `main`: contains all the sources 
    * `java`:
        * `uk.co.userservice`: main package
            * `api`: containing the entities used from the microservice
                * `User`: the main User entity used by the User, Search and Competition services
            * `health`: containing classes for health checks
                * `ServiceCheck`: basic health check that provides info on the number of users in the system
            * `persistence`:
                * `UserDB`: main persistence class, implemented as in-memory database. Operates on a HashMap to perform users operations.
            * `resources`:
                * `UserService`: main service, provides all the operations mentioned in the Architecture section.
                * `SearchService`: simple service listening for asynchronous messages incoming whenever a new user has been added.
                * `CompetitionService`: simple service listening for asynchronous messages incoming whenever a user updates its nickname.

    * `resources`:
        * `config.yaml`: the configuration file containing the environment params

* `test`: contains all the Unit test sources to for the main package
    * `java`:
        * `uk.co.userservice`: root package for the tests
            * `persistence`: unit tests for the persistence layer
            * `resources`: unit tests for the main User Service class
            * `serialization`: unit tests for serialization/deserialization of the User entity

# Running The Application

## Pre-requisites

* Java JDK 1.8 installed
* Maven 3.2.3 installed
* Git installed
* A REST tool, i.e. Postman, cURL, etc...

## Steps

* Clone the repository
* Move to the folder where the repository was cloned
* In the root folder, execute following command to package the example run: `mvn package`
(The `mvn package` will also execute automatically all the tests, if you want to manually run the tests use following command `mvn clean test`)
* To run the server move to the `target` folder that the mvn package command will create inside the root folder and type the following: `java -jar sample-userservice-core-1.0-SNAPSHOT.jar server ../src/main/resources/config.yaml`


# API Calls

The UserService class exposes following APIs:

Operation     | Path         | Method |Payload   | Response |
------------- | -------------|--------|---------|----------|
Add user  | `/user/add` | `PUT` |{"firstName": "testname","lastName": "testlastname","nickname": "testuser","password": "mypwd","email": "test@test.com","country": "uk"}| 200 OK: if operation is successfull
  | ||| 409 Conflict: if user with same email already exists
Get user by id (email)  | `/user/get/{emailId}` | `GET` | n/a        | 200 OK: the return body will be the User identified by emailId, JSON Format|
  |  |    |     | 404 Not found: if no user exists identified by emailId
Update user  | `/user/update` | `PUT` |{"firstName": "testname","lastName": "testlastname","nickname": "testuser","password": "mypwd","email": "test@test.com","country": "uk"}| 200 OK: if operation is successful
  | ||| 404 Not found: if no user exists identified by email value of the User sent as payload
Remove user  | `/user/remove/{emailId}` | `DELETE` | n/a        | 200 OK: if operation is successful|
  |  |        | | 404 Not found: if no user exists identified by emailId
Get all user  | `/user/all` | `GET` | n/a        | 200 OK: if operation is successful. The list of all the users will be returned in JSON format
Get all user by country | `/user/by-country/{countryIsocode}` | `GET` | n/a        | 200 OK: if operation is successful. The list of all the users for which the country field matches the `countryIsocode` will be returned in JSON format

## To post data into the application. 

It's possible to use any REST tool performing RESTful calls, e.g. Postman

It's possible also to use command line tools like CURL as following:

* To `POST`/`PUT` (example to add user via cURL):

`curl -H "Content-Type: application/json" -X PUT -d '{"firstName": "test","lastName": "user","nickname": "testuser","password": "mypwd","email": "test.user@email.com","country": "uk"}' http://localhost:8080/user/add`

* To `GET`:

`curl -H "Content-Type: application/json" -X GET http://localhost:8080/user/all`

# Healthchecks

The application also has a basic healthcheck that provides information on the number of current user, you can monitor the service by sending a `GET` request to `http://localhost:8081/healthcheck`